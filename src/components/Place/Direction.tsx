import React from "react";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import { SelectChangeEvent } from "@mui/material/Select";

type Props = {
  direction: string;
  label: string;
  onChange: (event: SelectChangeEvent) => void;
};
type State = {};

class Direction extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <FormControl>
        <InputLabel>{this.props.label}</InputLabel>
        <Select value={this.props.direction} onChange={this.props.onChange}>
          <MenuItem value={"NORTH"}>NORTH</MenuItem>
          <MenuItem value={"SOUTH"}>SOUTH</MenuItem>
          <MenuItem value={"EAST"}>EAST</MenuItem>
          <MenuItem value={"WEST"}>WEST</MenuItem>
        </Select>
      </FormControl>
    );
  }
}

export default Direction;
