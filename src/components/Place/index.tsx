import React from "react";
import Coordinate from "./Coordinate";
import Direction from "./Direction";
import Button from "@mui/material/Button";
import { SelectChangeEvent } from "@mui/material/Select";
import Grid from "@mui/material/Grid";

type Props = {
  onChangeX: (event: SelectChangeEvent) => void;
  onChangeY: (event: SelectChangeEvent) => void;
  onChangeDirection: (event: SelectChangeEvent) => void;
  onClick: (event: any) => void;
  x: string;
  y: string;
  direction: string;
};
type State = {};

class Place extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <Grid>
        <Coordinate
          label="x"
          onChange={this.props.onChangeX}
          coordinate={this.props.x}
        />
        <Coordinate
          label="y"
          onChange={this.props.onChangeY}
          coordinate={this.props.y}
        />
        <Direction
          label="direction"
          onChange={this.props.onChangeDirection}
          direction={this.props.direction}
        />
        <Button variant="outlined" onClick={this.props.onClick}>
          {" "}
          PLACE
        </Button>
      </Grid>
    );
  }
}

export default Place;
