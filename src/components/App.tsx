import React from "react";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import { SelectChangeEvent } from "@mui/material/Select";
import Typography from "@mui/material/Typography";
import Alert from "@mui/material/Alert";
import RobotDataService from "../api/robot";
import Place from "./Place";

type Props = {};
type State = {
  x: string | null;
  y: string | null;
  direction: string | null;
  selectedX: string;
  selectedY: string;
  selectedDirection: string;
  history: string[];
  errors: string[];
};
type IRobotState = {
  x: number;
  y: number;
  direction: string;
};

class App extends React.Component<Props, State> {
  state = {
    x: "0",
    y: "0",
    direction: "SOUTH",

    selectedX: "0",
    selectedY: "0",
    selectedDirection: "NORTH",
    history: [],
    errors: [],
  };

  constructor(props: Props) {
    super(props);
    this.onChangeX = this.onChangeX.bind(this);
    this.onChangeY = this.onChangeY.bind(this);
    this.onChangeDirection = this.onChangeDirection.bind(this);
    this.onClickMove = this.onClickMove.bind(this);
    this.onClickLeft = this.onClickLeft.bind(this);
    this.onClickRight = this.onClickRight.bind(this);
    this.onClickReport = this.onClickReport.bind(this);
    this.onClickReset = this.onClickReset.bind(this);
    this.onClickPlace = this.onClickPlace.bind(this);
    this.refreshState = this.refreshState.bind(this);
    this.refreshStateNotOnGrid = this.refreshStateNotOnGrid.bind(this);
  }

  componentWillMount() {
    this.onClickReport();
  }

  renderHistory() {
    return this.state.history.map((history, idx) => {
      return (
        <Grid item key={idx}>
          {history}
        </Grid>
      );
    });
  }

  renderError() {
    return this.state.errors.map((error, idx) => {
      return (
        <Alert severity="error" key={idx}>
          {error}
        </Alert>
      );
    });
  }

  refreshState(state: IRobotState, history: string) {
    this.setState({
      x: state.x.toString(),
      y: state.y.toString(),
      direction: state.direction,
      history: [...this.state.history, history],
    });
  }

  refreshStateNotOnGrid(history: string) {
    this.refreshState({
      "x": -1,
      "y": -1,
      "direction": "undefined",
    }, history)
  }

  setError(error: string, history: string) {
    this.setState({
      history: [...this.state.history, `${history} (Not successful: ${error})`],
      errors: [...this.state.errors, error],
    });
  }

  onChangeX(event: SelectChangeEvent) {
    this.setState({
      selectedX: event.target.value,
    });
  }

  onChangeY(event: SelectChangeEvent) {
    this.setState({
      selectedY: event.target.value,
    });
  }

  onChangeDirection(event: SelectChangeEvent) {
    this.setState({
      selectedDirection: event.target.value,
    });
  }

  onClickMove() {
    const log = "MOVE";
    RobotDataService.move()
      .then((data) => {
        this.refreshState(data.data, log);
      })
      .catch((error) => {
        this.setError(error.response.data, log);
      });
  }

  onClickLeft() {
    const log = "LEFT";
    RobotDataService.left()
      .then((data) => {
        this.refreshState(data.data, log);
      })
      .catch((error) => {
        this.setError(error.response.data, log);
      });
  }

  onClickRight() {
    const log = "RIGHT";
    RobotDataService.right()
      .then((data) => {
        this.refreshState(data.data, log);
      })
      .catch((error) => {
        this.setError(error.response.data, log);
      });
  }

  onClickReport() {
    RobotDataService.report()
      .then((data) => {
        const state = data.data;
        if (state.x === null && state.y === null && state.direction === null) {
          const log = `REPORT (Output: -1,-1,undefined). Robot is not on the grid!`;
          this.refreshStateNotOnGrid(log);
        } else {
          const log = `REPORT (Output: ${state.x},${state.y},${state.direction})`;
          this.refreshState(state, log);
        }
      })
      .catch((error) => {
        this.setError(error.response.data, "REPORT");
      });
  }

  onClickReset() {
    const log = "RESET";
    RobotDataService.reset()
      .then((data) => {
        const state = data.data;
        if (state.x === null && state.y === null && state.direction === null) {
          this.refreshStateNotOnGrid(log);
        } else {
          this.refreshState(state, log);
        }
      })
      .catch((error) => {
        this.setError(error.response.data, log);
      });
  }

  onClickPlace() {
    const log = `PLACE ${this.state.selectedX}, ${this.state.selectedY}, ${this.state.selectedDirection}`;
    RobotDataService.place({
      x: parseInt(this.state.selectedX),
      y: parseInt(this.state.selectedY),
      direction: this.state.selectedDirection,
    })
      .then((data) => {
        this.refreshState(data.data, log);
      })
      .catch((error) => {
        this.setError(error.response.data, log);
      });
  }

  render() {
    return (
      <div>
        <Typography variant="h2">Position</Typography>
        <Grid container direction={"column"} columnSpacing={2}>
          <Grid item>
            <Typography variant="h6">x: {this.state.x}</Typography>
          </Grid>
          <Grid item>
            <Typography variant="h6">y: {this.state.y}</Typography>
          </Grid>
          <Grid item>
            <Typography variant="h6">
              direction: {this.state.direction}
            </Typography>
          </Grid>
        </Grid>

        <Typography variant="h2">Controls</Typography>
        <Grid
          container
          rowSpacing={2}
          direction={"column"}
          justifyContent={"space-around"}
        >
          <Grid item>
            <Button variant="outlined" onClick={this.onClickMove}>
              {" "}
              MOVE
            </Button>
          </Grid>
          <Grid item>
            <Button variant="outlined" onClick={this.onClickLeft}>
              {" "}
              LEFT
            </Button>
          </Grid>
          <Grid item>
            <Button variant="outlined" onClick={this.onClickRight}>
              {" "}
              RIGHT
            </Button>
          </Grid>
          <Grid item>
            <Button variant="outlined" onClick={this.onClickReport}>
              {" "}
              REPORT
            </Button>
          </Grid>
          <Grid item>
            <Button variant="outlined" onClick={this.onClickReset}>
              {" "}
              RESET
            </Button>
          </Grid>
          <Grid item>
            <Place
              onChangeX={this.onChangeX}
              onChangeY={this.onChangeY}
              onChangeDirection={this.onChangeDirection}
              onClick={this.onClickPlace}
              x={this.state.selectedX}
              y={this.state.selectedY}
              direction={this.state.selectedDirection}
            />
          </Grid>
        </Grid>

        <Typography variant="h2"> History </Typography>
        <Grid
          container
          rowSpacing={0}
          direction={"column"}
          justifyContent={"space-around"}
        >
          {this.renderHistory()}
        </Grid>
      </div>
    );
  }
}

export default App;
