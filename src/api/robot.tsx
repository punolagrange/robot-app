import http from "./http-common";

type IRobotState = {
  x: number;
  y: number;
  direction: string;
};

class RobotDataService {
  left() {
    return http.post("/left");
  }

  right() {
    return http.post("/right");
  }

  move() {
    return http.post("/move");
  }

  report() {
    return http.get("/report");
  }

  reset() {
    return http.put("/reset");
  }

  place(state: IRobotState) {
    return http.put<IRobotState>("/place", state);
  }
}

export default new RobotDataService();
