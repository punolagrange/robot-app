#!/bin/bash
#############################################
#
# DO NOT DIRECTLY RUN THIS CODE IN LOCALHOST.
# THIS IS MEANT FOR BITBUCKET-PIPELINE TO RUN.
#
#############################################
env
(
    [ ! -z "${CI}" ] && 
    [ "$CI" == "true" ] &&
    [ ! -z ${BITBUCKET_COMMIT} ]
)|| { echo "This script is only meant to run on a CI pipeline. DO NOT RUN THIS ON YOUR LOCAL MACHINE"; exit 1; }
(
    [ ! -z "${TAG}" ] &&
    [ ! -z "${ECR_REPO}" ] && 
    [ ! -z "${TMP_DOCKER_TAG}" ] &&

    [ ! -z "${AWS_ACCESS_KEY_ID_ECR}" ] &&
    [ ! -z "${AWS_SECRET_ACCESS_KEY_ECR}" ] &&
    [ ! -z "${AWS_REGION_ECR}" ] &&
    [ ! -z "${AWS_ACCOUNT_ECR}" ] &&

    [ ! -z "${AWS_ACCESS_KEY_ID_ECS}" ] &&
    [ ! -z "${AWS_SECRET_ACCESS_KEY_ECS}" ] &&
    [ ! -z "${AWS_REGION_ECS}" ] && 

    [ ! -z "${CLUSTER_NAME}" ] &&
    [ ! -z "${SERVICE_NAME}" ] && 
    [ ! -z "${REACT_APP_API_BASE_URL}" ]
)|| { echo "PIPELINE ENV NOT SET PROPERLY."; exit 1; }

############
# PUSH IMAGE
############
TAG="${TAG}"
export AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID_ECR}"
export AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY_ECR}"
export AWS_REGION="${AWS_REGION_ECR}"
AWS_ACCOUNT_ECR="${AWS_ACCOUNT_ECR}"
ECR_REPO="${ECR_REPO}"

REPO_URI=${AWS_ACCOUNT_ECR}.dkr.ecr.${AWS_REGION_ECR}.amazonaws.com/${ECR_REPO}
COMMIT_HASH=$(echo ${BITBUCKET_COMMIT} | cut -c 1-12)

aws ecr get-login-password --region ${AWS_REGION_ECR} | \
    docker login --username AWS --password-stdin ${AWS_ACCOUNT_ECR}.dkr.ecr.${AWS_REGION_ECR}.amazonaws.com \
    || { echo "AWS ECR LOGIN ISSUE."; exit 1; }

docker build --build-arg REACT_APP_API_BASE_URL="${REACT_APP_API_BASE_URL}" -t ${TMP_DOCKER_TAG} .

docker tag ${TMP_DOCKER_TAG} ${REPO_URI}:${TAG} 
docker tag ${TMP_DOCKER_TAG} ${REPO_URI}:${COMMIT_HASH}

docker push ${REPO_URI}:${TAG}
docker push ${REPO_URI}:${COMMIT_HASH}

############
# UPDATE SVC
############
export AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID_ECS}"
export AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY_ECS}"
export AWS_REGION="${AWS_REGION_ECS}"
aws ecs update-service --region ${AWS_REGION_ECS} \
                       --cluster ${CLUSTER_NAME} \
                       --service ${SERVICE_NAME} \
                       --force-new-deployment
